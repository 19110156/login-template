import React from 'react'
import Auth from './pages/Auth/Auth'
import "./index.css"
const App: React.FC = () => {
  return (
   <div className={"app"} >
     <Auth/>
   </div>
  )
}

export default App
