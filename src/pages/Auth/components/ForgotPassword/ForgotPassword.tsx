import React from 'react'
import styles from './styles/forgot-password.module.scss'

const ForgotPassword=()=>{
    return(
        <div className={styles.forgotPassword}>
            <div className={styles.title}>
                <h2>Forgot Password</h2>
            </div>
            <div className={styles.body}>
                <p className={styles.label}>Nhập mail của bạn</p>
                <input type={"text"} placeholder={'Nhập mail của bạn '} />
                <span className={styles.validate}>Vui lòng kiểm tra email </span>
            </div>
            <div className={styles.bottom}>
                <button  type={"submit"}> Submit </button>
            </div>
        </div>
    )
}
export default ForgotPassword