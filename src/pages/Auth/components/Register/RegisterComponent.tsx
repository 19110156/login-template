
import styles from "./styles/register.module.scss"

const RegisterComponent=(props:any)=>{



    return(

        <div className={styles.login}>
            <form>
                <div className={styles.loginForm}>
                    <div className={styles.loginFormTitle}>
                        <h2>Register</h2>
                        <p>Enter your details below</p>
                    </div>
                    <div className={styles.loginFormSocial}>
                        <span className={styles.color}><i className="fa-brands fa-instagram"/></span>
                        <span><i className="fa-brands fa-facebook"/></span>
                        <span><i className="fa-brands fa-google"/></span>
                    </div>
                    <div className={styles.loginFormLine}>
                        <span className={styles.line}/> <span className={styles.or}>OR</span><span className={styles.line}/>
                    </div>
                    <div className={styles.loginFormBody}>
                        <div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"text"} placeholder='Enter your name'

                                />
                                <span> "Email is validate *" </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"password"} placeholder='Enter your password'

                                />
                                <span> "Email is validate *" </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"text"} placeholder='Enter your phone'

                                />
                                <span>Email is validate *"</span>
                            </div>
                        </div>
                        <div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"text"} placeholder='Enter your email'
                                />
                                <span>Email is validate * </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <input type={"password"} placeholder='Enter confirm password'/>
                                <span>Email is validate * </span>
                            </div>
                            <div className={styles.loginFormBodyItem}>
                                <select >
                                    <option >Bạn là ?</option>
                                    <option value={"teacher"}>Teacher</option>
                                    <option value={"student"}>Student</option>
                                    <option value={"leader"}>Leader</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className={styles.loginFormBottom}>
                        <div className={styles.loginFormBottomAction}>
                            <button > Register</button>
                        </div>
                        <div  className={styles.loginFormBottomRegisterOrLogin}>
                            <p>Don't must have account ?  <span onClick={props.RegisterToLogin}>Click to Login</span></p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    )
}

export  default  RegisterComponent;