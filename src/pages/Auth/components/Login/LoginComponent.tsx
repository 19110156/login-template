import React, { useEffect } from 'react'
import styles from  './styles/login.module.scss'


const LoginComponent = (props:any) => {



    useEffect(()=>{

    },[])

  return (
        <div className={styles.login}>
            <form  autoComplete="off">
                  <div className={styles.loginForm}>
                        <div className={styles.loginFormTitle}>
                              <h2>Sign Place </h2>
                            <p>Enter your details below</p>
                        </div>
                      <div className={styles.loginFormSocial}>
                          <span className={styles.color}><i className="fa-brands fa-instagram"/></span>
                          <span><i className="fa-brands fa-facebook"/></span>
                          <span><i className="fa-brands fa-google"/></span>
                      </div>
                      <div className={styles.loginFormLine}>
                          <span className={styles.line}/> <span className={styles.or}>OR</span><span className={styles.line}/>
                      </div>
                        <div className={styles.loginFormBody}>
                              <div className={styles.loginFormBodyItem}>
                                    <input
                                        type={"text"}
                                        placeholder='Email Address'
                                           />
                                    <span> "Email is validate *" </span>
                              </div>
                              <div className={styles.loginFormBodyItem}>
                                    <input
                                        type={"password"}
                                        placeholder='Password'
                                        autoComplete="off"

                                    />
                                    <span>"Password is validate *" </span>
                              </div>
                        </div>
                        <div className={styles.loginFormBottom}>
                              <div className={`${styles.loginFormBottomAction} item-center`}>
                                  <button type={"submit"}><i className="fa-solid fa-arrow-right-to-arc"/>Login</button>
                              </div>
                              <div  className={styles.loginFormBottomRegisterOrLogin}>
                                  <p>Don't must have account ? <span onClick={props.ChangLoginToRegister}>Click to register</span></p>
                                  <p ><span onClick={props.HandleChangLoginForgot}>Forgot password</span></p>
                              </div>
                        </div>
                  </div>
            </form>
        </div>
  )
}
 
export default LoginComponent



