import React from 'react'
import styles from './styles/auth.module.scss'
import LoginComponent from "./components/Login/LoginComponent"
import RegisterComponent from "./components/Register/RegisterComponent"
import LogoSPKT from '../../asset/images/illustration_login.png'
// import Image from "../../components/Image/Image";
const Auth=()=>{

    const [isLogin,setIsLogin]=React.useState("login")

    function HandleChangLoginRegister(){
        setIsLogin("register")
    }
    function HandleChangRegisterToLogin(){
        setIsLogin("login")
    }
    function HandleChangLoginForgot(){
        setIsLogin("forgot")
    }

    return(

            <div className={`${styles.loginContainer} item-center`}>
                <div className={`${styles.loginMain}`}>
                    <div className={styles.img}>
                        {/*<div className={styles.logo}>*/}
                        {/*    <Image image={LogoSPKT}/>*/}
                        {/*</div>*/}
                        <div className={`${styles.logo} background-image`} style={{backgroundImage:`url("${LogoSPKT}")`}}/>
                        <div className={styles.title}>
                            <h2>Chào mừng bạn đã trở lại</h2>
                        </div>
                        {/*<div className={`${styles.logoImage} background-image`} style={{backgroundImage:`url("${LoginImage}")`}}/>*/}
                    </div>
                    <div className={styles.content}>
                        {isLogin==="login" && <LoginComponent ChangLoginToRegister={HandleChangLoginRegister}  HandleChangLoginForgot={HandleChangLoginForgot}v  />}
                        {isLogin==="register" && <RegisterComponent RegisterToLogin={HandleChangRegisterToLogin} setIsLogin={setIsLogin}/>}

                    </div>
                </div>
            </div>
    )
}
export default  Auth