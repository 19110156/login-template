interface ISideBar{
    path:String
    title:String
    // icon:HTMLElement
}
const DataSidebarAdmin:ISideBar[]=[
    {
        path:"Home",
        title:"Home",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Teacher",
        title:"Teacher",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Student",
        title:"Teacher",
        // icon:<i class="fa-regular fa-book"/>
    },
]
const DataSidebarTeacher:ISideBar[]=[
    {
        path:"Home",
        title:"Home",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Topic",
        title:"Topic",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"AcceptTopic",
        title:"Báo cáo",
        // icon:<i class="fa-regular fa-book"/>
    }
]
const DataSidebarLeader:ISideBar[]=[
    {
        path:"Topic",
        title:"Topic",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Teacher",
        title:"Teacher",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Student",
        title:"Student",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"ViewReport",
        title:"Báo cáo",
        // icon:<i class="fa-regular fa-book"/>
    }  ,
    {
        path:"Core",
        title:"Điểm số ",
        // icon:<i class="fa-regular fa-book"/>
    }
]
const DataSidebarStudent:ISideBar[]=[
    {
        path:"Teacher",
        title:"Báo cáo ",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Student",
        title:"Nộp báo cáo",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Topic",
        title:"Đề tài ",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Group",
        title:"Hội đồng ",
        // icon:<i class="fa-regular fa-book"/>
    }
]

const SideBarDB={
    DataSidebarAdmin,
    DataSidebarLeader,
    DataSidebarStudent,
    DataSidebarTeacher
}
export default SideBarDB
